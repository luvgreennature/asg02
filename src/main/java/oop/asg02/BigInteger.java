package oop.asg02;

public class BigInteger {
	// A new BigInteger is Zero in default
	private int[] digit = new int[50];
	private int sign = 0;
	private int size = 0;
	
    public BigInteger (int init) {
		// Determine sign
		if (init < 0) {
			sign = -1;
			init *= -1; // init need to be positive to pick digit off
		}
		else if (init == 0) {
			sign = 0;
		}
		else {
			sign = 1;
		}
		
		// Store digits from init in Little Endian form
		while (init != 0) {
			digit[size] = init % 10;
			size++;
			init /= 10;
		}
    }

    public BigInteger (String init) {
		if (init.charAt (0) == '-') {
			sign = -1;
		}
		
		for (int i = init.length() - 1; i >= 0; i--) {
			digit[size] = init.charAt (i) - '0';
			size++;
		}
		if (sign == -1) {
			size--;
		}
		// The leading zeroes in string are deleted
		while (size > 0 && digit[size - 1] == 0) {
			size--;
		}
		
		if (size == 0) {
			sign = 0;
		}
		else {
			sign = 1;
		}
	}
	
    public String toString() {
		char[] container = new char[51];
		int containerSize = 0;
		
        if (size == 0) {
			return "0";
		}
		else {
			if (sign == -1) {
				container[containerSize] = '-';
				containerSize++;
			}
			
			while (containerSize < size) {
				container[containerSize] = (char) ('0' + digit[size - 1 - containerSize]);
				containerSize++;
			}
			
			return String.copyValueOf (container, 0, containerSize);
		}
    }
	
    public boolean equals (Object other) {
        return toString().equals (other.toString());
    }

    public long toLong() {
		long result = 0, coefficient= 1;
		
		for (int i = 0; i < size; i++) {
			result += digit[i] * coefficient;
			coefficient *= 10;
		}
		result *= sign;
        
        return result; 
    }

    public BigInteger add (BigInteger other) {
		String otherInString = other.toString();
		int[] addendDigit = new int[51];
		char[] sum = new char[51];
		int addendDigitSize = 0, addendSign = 1, productOfSigns,
			  sumSize = 0, i;
		boolean carry = false;
		
		if (otherInString.charAt (0) == '-') {
			addendSign = -1;
		}
		else if (otherInString.charAt (0) == '0') {
			addendSign = 0;
		}
		
		// Store digits from otherInString into addendDigit in Little endian form
		for (i = otherInString.length() - 1; i >= 0; i--) {
			addendDigit[addendDigitSize] = otherInString.charAt (i) - '0';
			addendDigitSize++;
		}
		
		// If otherInString begins with '-', then the last digit is calculated by it
		if (addendSign == -1) {
			addendDigitSize--;
		}
		
		productOfSigns = sign * addendSign;
		if (productOfSigns == 1) {
			if (size < addendDigitSize) {
				for (i = 0; i < size; i++) {
					addendDigit[i] += digit[i];
					if (carry) {
						addendDigit[i]++;
						carry = false;
					}
					
					if (addendDigit[i] > 9) {
						carry = true;
						addendDigit[i] %= 10;
					}
				}
				while (carry) {
					addendDigit[i]++;
					carry = false;
					if (addendDigit[i] > 9) {
						carry = true;
						addendDigit[i] %= 10;
					}
					i++;
				}
			}
			else {
				for (i = 0; i < addendDigitSize; i++) {
					addendDigit[i] += digit[i];
					if (carry) {
						addendDigit[i]++;
						carry = false;
					}
					if (addendDigit[i] > 9) {
						carry = true;
						addendDigit[i] %= 10;
					}
				}
				while (carry) {
					addendDigit[i] = digit[i] + 1;
					carry = false;
					if (addendDigit[i] > 9) {
						carry = true;
						addendDigit[i] %= 10;
					}
					i++;
				}
				while (i < size) {
					addendDigit[i] = digit[i];
					i++;
				}
				addendDigitSize = size;
			}
			
			if (sign == -1) {
				sum[sumSize] = '-';
				sumSize++;
			}
			if (carry) {
				sum[sumSize] = '1';
				sumSize++;
			}
			for (i = addendDigitSize - 1; i >= 0; i--) {
				sum[sumSize] = (char) ('0' + addendDigit[i]);
				sumSize++;
			}
			
			return new BigInteger (String.copyValueOf (sum, 0, sumSize));
		}
		else if (productOfSigns == 0) {
			if (sign == 0) {
				return other;
			}
			else {
				return this;
			}
		}
		else {
			if (sign == 1) {
				return this.subtract (other);
			}
			else {
				return other.subtract (this);
			}
		}
	}

    public BigInteger subtract (BigInteger other) {
		String otherInString = other.toString();
		int[] subtrahendDigit = new int[51];
		char[] different = new char[51];
		int subtrahendSign = 1, subtrahendDigitSize = 0, i,
			  productOfSigns, differentSize = 0;
		// Presume absolute of this is greater than absolute of other
		boolean notLessThan = true, carry = false;
		
		if (otherInString.charAt (0) == '-') {
			subtrahendSign = -1;
		}
		else if (otherInString.charAt (0) == '0') {
			subtrahendSign = 0;
		}
		
		for (i = otherInString.length() - 1; i >= 0; i--) {
			subtrahendDigit[subtrahendDigitSize] = otherInString.charAt (i) - '0';
			subtrahendDigitSize++;
		}
		
		if (subtrahendSign == -1) {
			subtrahendDigitSize--;
		}
		
		if (size == subtrahendDigitSize)  {
			for (i = size - 1; i >= 0; i--) {
				if (digit[i] < subtrahendDigit[i]) {
					notLessThan = false;
					break;
				}
			}
		}
		else if (size < subtrahendDigitSize) {
			notLessThan = false;
		}
		
		productOfSigns = sign * subtrahendSign;
		if (productOfSigns == 1) {
			if (notLessThan) {
				for (i = 0; i < subtrahendDigitSize; i++) {
					subtrahendDigit[i] = digit[i] - subtrahendDigit[i];
					if (carry) {
						subtrahendDigit[i]--;
						carry = false;
					}
					if (subtrahendDigit[i] < 0) {
						carry = true;
						subtrahendDigit[i] += 10;
					}
				}
				while (carry) {
					subtrahendDigit[i] = digit[i] - 1;
					carry = false;
					if (subtrahendDigit[i] < 0) {
						carry = true;
						subtrahendDigit[i] += 10;
					}
					i++;
				}
				while (i < size) {
					subtrahendDigit[i] = digit[i];
					i++;
				}
				subtrahendDigitSize = size;
			}
			else {
				productOfSigns *= -1;
				
				for (i = 0; i < size; i++) {
					subtrahendDigit[i] -= digit[i];
					if (carry) {
						subtrahendDigit[i]--;
						carry = false;
					}
					if (subtrahendDigit[i] < 0) {
						carry = true;
						subtrahendDigit[i] += 10;
					}
				}
				while (carry) {
					subtrahendDigit[i]--;
					carry = false;
					if (subtrahendDigit[i] < 0) {
						carry = true;
						subtrahendDigit[i] += 10;
					}
					i++;
				}
			}
			
			if (productOfSigns == -1) {
				different[differentSize] = '-';
				differentSize++;
			}
			for (i = subtrahendDigitSize - 1; i >= 0; i--) {
				different[differentSize] = (char) ('0' + subtrahendDigit[i]);
				differentSize++;
			}
			
			return new BigInteger (String.copyValueOf (different, 0, differentSize));
		}
		else if (productOfSigns == 0) {
			if (sign == 0) {
				if (subtrahendSign == -1) {
					return other;
				}
				else if (subtrahendSign == 1) {
					return new BigInteger ("-".concat (otherInString));
				}
				else {
					return new BigInteger ("0");
				}
			}
			else {
				return this;
			}
		}
		else {
			if (sign == 1) {
				return add (other);
			}
			else {
				return add (new BigInteger ("-".concat (otherInString)));
			}
		}
	}
}
