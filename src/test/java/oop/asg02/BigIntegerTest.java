package oop.asg02;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Unit test for BigInteger class.
 */
public class BigIntegerTest
{
    @Test
    public void testCreateBigIntegerFromInt()
    {
        BigInteger bigInt = new BigInteger(1);
        assertEquals("1", bigInt.toString());
    }

    @Test
    public void testCreateBigIntegerFromStringWithAllZeros()
    {
        BigInteger bigInt = new BigInteger("00");
        assertEquals("0", bigInt.toString());
    }

    @Test
    public void testConvertToLongWhenNotOverRange()
    {
        BigInteger bigInt = new BigInteger("1");
        assertEquals(1L, bigInt.toLong());
    }
    
    @Test
    public void testEquality()
    {
        BigInteger bigInt1 = new BigInteger(1);
        BigInteger bigInt2 = new BigInteger(1);
        BigInteger bigInt3 = new BigInteger("1");
                
        assertEquals(bigInt1, bigInt2);
        assertEquals(bigInt1, bigInt3);
    }
    
    @Test
    public void testInequality()
    {
        BigInteger bigInt1 = new BigInteger(0);
        BigInteger bigInt2 = new BigInteger(1);
                
        assertFalse(bigInt1.equals(bigInt2));
    }
    
    @Test
    public void testAdditionWithoutCarryOn()
    {
        BigInteger bigInt1 = new BigInteger("11111111111");
        BigInteger bigInt2 = new BigInteger("1");
        BigInteger sum = bigInt1.add(bigInt2);
        
        assertEquals(new BigInteger("11111111112"), sum);
    }
   
	@Test
    public void testAdditionWithCarryOn()
    {
        BigInteger bigInt1 = new BigInteger("11111111111");
        BigInteger bigInt2 = new BigInteger("9");
        BigInteger sum = bigInt1.add(bigInt2);
        
        assertEquals(new BigInteger("11111111120"), sum);
    }
    
    @Test
    public void testAdditionWithNegs() {
		BigInteger bi1 = new BigInteger ("-11111111111");
		BigInteger bi2 = new BigInteger ("-1");
		BigInteger sum = bi1.add (bi2);
		
		assertEquals (sum, new BigInteger ("-11111111112"));
	}
	
    @Test
    public void testSubtractionWithoutCarryOn()
    {
        BigInteger bigInt1 = new BigInteger("11111111111");
        BigInteger bigInt2 = new BigInteger("11111111111");
        BigInteger difference = bigInt1.subtract(bigInt2);
        
        assertEquals(new BigInteger("0"), difference);
    }
    
    @Test
    public void testSubtractionWithCarryOn()
    {
        BigInteger bigInt1 = new BigInteger("11111111111");
        BigInteger bigInt2 = new BigInteger("2");
        BigInteger difference = bigInt1.subtract(bigInt2);
        
        assertEquals(new BigInteger("11111111109"), difference);
    }
    
    @Test
    public void testSubWithMinuendLessThanSubtrahend() {
		BigInteger bi1 = new BigInteger ("11111101111");
		BigInteger bi2 = new BigInteger ("11111111111");
		BigInteger diff = bi1.subtract (bi2);
		
		assertEquals (diff, new BigInteger ("-10000"));
	}
}
